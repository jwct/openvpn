# setup open vpn in ubuntu

## new installation
0. In launch a ubuntu VM in EC2

1. install docker and docker-compose
```
sudo bash
curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh
curl -L "https://github.com/docker/compose/releases/download/$(curl -sL https://api.github.com/repos/docker/compose/releases/latest | grep tag_name | cut -d'"' -f 4)/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose
```

2. change to root and config docker-compose
```
mkdir /root/open_vpn && cd /root/open_vpn
cat << EOF > docker-compose.yml
version: '2'
services:
  openvpn:
    cap_add:
     - NET_ADMIN
    image: kylemanna/openvpn
    container_name: openvpn
    ports:
     - "1194:1194/udp"
    restart: always
    volumes:
     - ./openvpn-data/conf:/etc/openvpn
EOF
```

3. generate openvpn config
- if no openvpn config is created then generate openvpn config (change the url when needed)
```
docker-compose run --rm openvpn ovpn_genconfig -u udp://openvpn.josephw.work
docker-compose run --rm openvpn ovpn_initpki
```
- if you have a openvpn config backup, then sftp the config file to server. After extracted the config tar ball, remember to change owner and group to docker user.
```
chown -R root .
chgrp -R root .
```

4. export opvn file (change the client name when needed)
```
export CLIENTNAME="openvpn_josephw_work"
docker-compose run --rm openvpn easyrsa build-client-full "$CLIENTNAME"
docker-compose run --rm openvpn ovpn_getclient "$CLIENTNAME" > "$CLIENTNAME.ovpn"
```
download openvpn file with sftp

5. run openvpn
```
docker-compose up -d openvpn
```
